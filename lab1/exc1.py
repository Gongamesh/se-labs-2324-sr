def count_evens(list):
    """Return the number of uppercase letters inside the string.

    Arguments:
    string - input string which will be checked
    """

    count = 0

    for number  in list:
        if(number%2==0):
            count += 1

    print(count)

def main():
    count_evens([2, 1, 2, 3, 4])
    count_evens([2, 2, 0])
    count_evens([1, 3, 5])


if __name__ == "__main__":
    main()
