def has_adjacent_2(nums):
    num_list = [int(num) for num in nums.split(",")]

    for i in range(len(num_list) - 1):
        if num_list[i] == 2 and num_list[i + 1] == 2:
            return True

    return False

def main():
    print(has_adjacent_2("1, 2, 2, 4, 100"))
    print(has_adjacent_2("1, 1, 5, 5, 10, 8, 7"))
    print(has_adjacent_2("-10, -4, -2, -4, -2, 0"))

if __name__ == "__main__":
    main()
