def main():

    with open('lab2/ex2-text.csv', 'r', encoding='utf-8') as f:
        txt = f.readlines()

    data = [line.strip().split(',') for line in txt[1:]]

    employees = [row[0] for row in data]
    job_titles = [row[1] for row in data]
    offices = [row[3] for row in data]

    with open('lab2/ex2-employees.txt', 'w', encoding='utf-8') as employees_file:
        for employee, job_title in zip(employees, job_titles):
            employees_file.write(f"{employee},{job_title}\n")

    with open('lab2/ex2-locations.txt', 'w', encoding='utf-8') as locations_file:
        for employee, office in zip(employees, offices):
            locations_file.write(f"{employee},{office}\n")

if __name__ == "__main__":
    main()        
